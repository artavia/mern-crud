// BASE SETUP
// =============================================
const express = require("express");
const todoRouter = express.Router();

// MODEL DEFINITION
// =============================================
const Todo = require("../custom_models_mongoose/todo.model");

// ROUTING ASSIGNMENTS
// =============================================
todoRouter.route("/").get( ( req, res ) => {
  Todo.find( (err, todos) => {
    if(err){
      console.log( "err" , err );
    }
    res.json( todos );
  } );
} );

todoRouter.route("/:id").get( ( req, res ) => {
  
  let id = req.params.id;

  Todo.findById( id, ( err, todo ) => {
    if(err){
      console.log( "err" , err );
    }
    res.json(todo);
  } );
} );

todoRouter.route("/add").post( ( req, res ) => {

  /*
  {
    "todo_description" : "Eat more ice cream"
    , "todo_responsible" : "Lucho"
    , "todo_priority" : "Low"
    , "todo_completed" : false
  }
  */

  // console.log( "req.body" , req.body ); 
  
  let todo = new Todo( req.body );
  todo.save().then( todo => {
    return res.status(200).json( { 'todo': 'todo added successfully' } );
  } ).catch( err => {
    return res.status(400).send( 'Add new todo failed, doofus!' );
  } );

} );

todoRouter.route("/update/:id").post( ( req, res ) => {
  
  let id = req.params.id;

  Todo.findById( id, ( err, todo ) => {
    if(!todo){
      return res.status(404).send("Data not found... DOOFUS!")
    }
    todo.todo_description = req.body.todo_description;
    todo.todo_responsible = req.body.todo_responsible;
    todo.todo_priority = req.body.todo_priority;
    todo.todo_completed = req.body.todo_completed;

    todo.save().then( todo => { return res.status(200).json('Todo updated') } ).catch( err => {
      return res.status(400).send( 'Update has failed, doofus!' );
    } );
  } );
} );

// todoRouter.route("/delete/:id").post( ( req, res ) => {
todoRouter.route("/delete/:id").get( ( req, res ) => {

  let idObjParameter = { _id: req.params.id };
  Todo.findByIdAndRemove( idObjParameter, ( err, todo ) => {
    if(err){
      console.log( "err" , err );
      res.json(err);
    }
    res.json( "Todo removed successfully" );
  } );

} );

module.exports = todoRouter;