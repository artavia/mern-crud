import React from 'react';
import { NavLink } from 'react-router-dom';
import { TodoService } from './todo-service';

class CreateTodo extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      todo_description: ""
      , todo_responsible: ""
      , todo_priority: ""
      , todo_completed: false
    };

    this.todoService = new TodoService();
    this.onChangeTodoDescription = this.onChangeTodoDescription.bind(this);
    this.onChangeTodoResponsible = this.onChangeTodoResponsible.bind(this);
    this.onChangeTodoPriority = this.onChangeTodoPriority.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.noOpLink = this.noOpLink.bind(this);
  }

  onChangeTodoDescription(event){
    this.setState( {
      todo_description: event.target.value
    } );
  }

  onChangeTodoResponsible(event){
    this.setState( {
      todo_responsible: event.target.value
    } );
  }

  onChangeTodoPriority(event){
    this.setState( {
      todo_priority: event.target.value
    } );
  }

  handleSubmit(event){

    event.preventDefault();

    // console.log(`Form submitted ~ Todo Description: ${this.state.todo_description}`);
    // console.log(`Form submitted ~ Todo Responsible: ${this.state.todo_responsible}`);
    // console.log(`Form submitted ~ Todo Priority: ${this.state.todo_priority}`);
    // console.log(`Form submitted ~ Todo Completed: ${this.state.todo_completed}`);

    const newTodo = {
      todo_description: this.state.todo_description
      , todo_responsible: this.state.todo_responsible
      , todo_priority: this.state.todo_priority
      , todo_completed: this.state.todo_completed
    };

    this.todoService.createTodo( this.props.history, newTodo ); 

  }

  noOpLink(event){
    event.preventDefault(); 
  }

  render(){
    
    let element = (
      <>
        <div className="jumbotron">
          <div className="container">
            <h1 className="display-3">Create New Todo</h1>
            <p><NavLink className="btn btn-primary btn-lg" onClick={ this.noOpLink } to="#" role="button">Learn more &raquo;</NavLink></p>
          </div>
        </div>
  
        <div className="container">
  
          <form onSubmit={ this.handleSubmit }>
            
            <div className="form-group">
            <label htmlFor="description" className="form-check-label">Description</label>
              <input type="text" id="description" className="form-control" value={ this.state.todo_description } onChange={ this.onChangeTodoDescription } />
            </div>

            <div className="form-group">
              <label htmlFor="responsibility" className="form-check-label">Responsible</label>
              <input type="text" id="responsibility" className="form-control" value={ this.state.todo_responsible } onChange={ this.onChangeTodoResponsible } />
            </div>

            <div className="form-group">
              <div className="form-check form-check-inline">
                <input className="form-check-input" type="radio" name="priorityOptions" id="priorityLow" value="Low" checked={ this.state.todo_priority==="Low"} onChange={ this.onChangeTodoPriority } />
                <label htmlFor="priorityLow" className="form-check-label">Low</label>
              </div>

              <div className="form-check form-check-inline">
                <input className="form-check-input" type="radio" name="priorityOptions" id="priorityMedium" value="Medium" checked={ this.state.todo_priority==="Medium"} onChange={ this.onChangeTodoPriority } />
                <label htmlFor="priorityMedium" className="form-check-label">Medium</label>
              </div>

              <div className="form-check form-check-inline">
                <input className="form-check-input" type="radio" name="priorityOptions" id="priorityHigh" value="High" checked={ this.state.todo_priority==="High"} onChange={ this.onChangeTodoPriority } />
                <label htmlFor="priorityHigh" className="form-check-label">High</label>
              </div>
            </div>

            <div className="form-group">
              <input type="submit" value="Create Todo" className="btn btn-primary" />
            </div>

          </form>
  
        </div>
      </>
    );

    return element;
  }
}

export {CreateTodo};