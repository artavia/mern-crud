import React from 'react';
import { NavLink } from 'react-router-dom';
import { TodoService } from './todo-service';

class EditTodo extends React.Component{

  constructor(props){
    super(props);
    
    this.state = {
      todo_description: ""
      , todo_responsible: ""
      , todo_priority: ""
      , todo_completed: false
    };

    this.todoService = new TodoService();
    this.onChangeTodoDescription = this.onChangeTodoDescription.bind(this);
    this.onChangeTodoResponsible = this.onChangeTodoResponsible.bind(this);
    this.onChangeTodoPriority = this.onChangeTodoPriority.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onChangeTodoCompleted = this.onChangeTodoCompleted.bind(this);
    this.noOpLink = this.noOpLink.bind(this);
  }

  componentDidMount(){ 
    this.loadIt( this, this.props.match.params.id );
  }

  loadIt( pm , id ){
    this.todoService.getOneTodo(pm,id);
  }

  onChangeTodoDescription(event){
    this.setState( {
      todo_description: event.target.value
    } );
  }

  onChangeTodoResponsible(event){
    this.setState( {
      todo_responsible: event.target.value
    } );
  }

  onChangeTodoPriority(event){
    this.setState( {
      todo_priority: event.target.value
    } );
  }

  onChangeTodoCompleted(event){
    this.setState( ( prevState, props ) => { 
      return { todo_completed: !prevState.todo_completed };
    } );
  }

  onSubmit(event){
    
    event.preventDefault();

    const { todo_description , todo_responsible, todo_priority, todo_completed } = this.state;

    const todoObject = { todo_description: todo_description, todo_responsible: todo_responsible, todo_priority: todo_priority, todo_completed: todo_completed };
    
    this.todoService.updateTodo( this.props, todoObject );
  }
  
  noOpLink(event){
    event.preventDefault(); 
  }

  render(){
    let element = (
      <>
        <div className="jumbotron">
          <div className="container">
            <h1 className="display-3">Edit Todo Item</h1>   
            <p><NavLink className="btn btn-primary btn-lg" onClick={ this.noOpLink } to="#" role="button">Learn more &raquo;</NavLink></p>
          </div>
        </div>
  
        <div className="container">
  
        <form onSubmit={ this.onSubmit }>
            
            <div className="form-group">
            <label htmlFor="description" className="form-check-label">Description</label>
              <input type="text" id="description" className="form-control" value={ this.state.todo_description } onChange={ this.onChangeTodoDescription } />
            </div>

            <div className="form-group">
              <label htmlFor="responsibility" className="form-check-label">Responsible</label>
              <input type="text" id="responsibility" className="form-control" value={ this.state.todo_responsible } onChange={ this.onChangeTodoResponsible } />
            </div>

            <div className="form-group">
              <div className="form-check form-check-inline">
                <input className="form-check-input" type="radio" name="priorityOptions" id="priorityLow" value="Low" checked={ this.state.todo_priority==="Low"} onChange={ this.onChangeTodoPriority } />
                <label htmlFor="priorityLow" className="form-check-label">Low</label>
              </div>

              <div className="form-check form-check-inline">
                <input className="form-check-input" type="radio" name="priorityOptions" id="priorityMedium" value="Medium" checked={ this.state.todo_priority==="Medium"} onChange={ this.onChangeTodoPriority } />
                <label htmlFor="priorityMedium" className="form-check-label">Medium</label>
              </div>

              <div className="form-check form-check-inline">
                <input className="form-check-input" type="radio" name="priorityOptions" id="priorityHigh" value="High" checked={ this.state.todo_priority==="High"} onChange={ this.onChangeTodoPriority } />
                <label htmlFor="priorityHigh" className="form-check-label">High</label>
              </div>
            </div>

            <div className="form-group">
              <div className="form-check form-check-inline">
                <input type="checkbox" className="form-check-input" id="completedCheckbox" name="completedCheckbox" onChange={ this.onChangeTodoCompleted } checked={ this.state.todo_completed } value={ this.state.todo_completed } />
                <label htmlFor="completedCheckbox" className="form-check-label">Completed</label>
              </div>
            </div>

            <div className="form-group">
              <input type="submit" value="Update Todo" className="btn btn-warning" />
            </div>

          </form>
  
        </div>
      </>
    );
    return element;
  }
}

export {EditTodo};