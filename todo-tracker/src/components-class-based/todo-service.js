import axios from 'axios';

class TodoService {

  constructor(){
    this.baseUrl = "http://localhost:4000/todos";
  }

  getAllTodos( pm ){ // console.log( "pm" , pm ); // this

    axios.get( `${ this.baseUrl }` )
    .then( ( response ) => { // console.log( "response" , response );
      pm.setState( { todos: response.data } );
    } )
    .catch( (err) => console.log( "err", err ) );
  } 

  createTodo( h, obj ){ 
    // console.log( "h - this.props.history" , h ); // this.props.history
    // console.log( "obj - newTodo" , obj ); // newTodo 
    
    axios.post( `${this.baseUrl}/add` , obj )
    .then( response => { // console.log( "response.data" , response.data );
      h.push('/');
    } )
    .catch( (err) => console.log( "err", err ) ); 
  } 

  getOneTodo( pm, id ){     
    // console.log( "pm" , pm ); // this
    // console.log( "id" , id ); // id
    
    axios.get( `${this.baseUrl}/${id}` )
    .then( ( response ) => {
      const { todo_description , todo_responsible, todo_priority , todo_completed } = response.data;
      pm.setState( {
        todo_description: todo_description, todo_responsible: todo_responsible, todo_priority: todo_priority, todo_completed: todo_completed
      } );
    } )
    .catch( (err) => console.log( "err", err ) );
  }

  updateTodo( props, obj ){ // console.log( "props" , props ); // this.props
    
    axios.post( `${this.baseUrl}/update/${props.match.params.id}`, obj )
    .then( ( response ) => { // console.log( "response.data" , response.data );
      props.history.push( "/" );
    } )
    .catch( (err) => console.log( "err", err ) ); 
  } 

  deleteTodo( pm ){ // console.log( "pm" , pm ); // this
    
    let id = pm.props.todo._id; // console.log( "id", id );
    // console.log( "pm.state.redirect", pm.state.redirect );

    axios.get( `${ this.baseUrl }/delete/${id}` )
    .then( (response) => { // console.log( "response.data", response.data );
      pm.setState( ( prevState, props ) => {
        if( prevState.redirect === false ){
          return { redirect: true };
        }
      } );
    })
    .catch( (err) => console.log( "err", err ) );
    
  }

}

export { TodoService };