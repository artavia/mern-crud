import React from 'react';
import { NavLink, Redirect } from 'react-router-dom';
import { TodoService } from './todo-service';

class Todo extends React.Component{
  
  constructor(props){
    super(props);
    this.props = props;
    this.state = { redirect: false };

    this.todoService = new TodoService();
    this.deleteRecord = this.deleteRecord.bind(this);
  }

  noOpLink(event){
    event.preventDefault(); 
  }

  deleteRecord(event){
    event.preventDefault();
    this.todoService.deleteTodo( this ); // console.log( "this.props.todo._id", this.props.todo._id ); 
  }

  render(){
    const { redirect } = this.state;

    if( redirect ){
      return ( <Redirect to='/' /> );
    }

    let element = (
      <tr>
        <td className={ this.props.todo.todo_completed ? 'completed' : '' } >{this.props.todo.todo_description}</td>
        <td className={ this.props.todo.todo_completed ? 'completed' : '' }>{this.props.todo.todo_responsible}</td>
        <td className={ this.props.todo.todo_completed ? 'completed' : '' }>{this.props.todo.todo_priority}</td>
        
        <td>
          <NavLink to={`/edit/${this.props.todo._id}`}>
            Edit
          </NavLink>
        </td>
        <td>
          <NavLink className="danger" onClick={ this.deleteRecord } to="#" >
            Delete
          </NavLink>
        </td>
      </tr>
    );
    return element;
  }
}

export { Todo };