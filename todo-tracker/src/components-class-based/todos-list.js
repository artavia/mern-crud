import React from 'react';
import { NavLink } from 'react-router-dom';
import { Todo } from './todo';
import { TodoService } from './todo-service';

class TodosList extends React.Component{
  
  constructor(props){
    super(props);

    this.state = { todos: [] };
    this.todoService = new TodoService(); 
    this.noOpLink = this.noOpLink.bind(this);
    this.mapTodos = this.mapTodos.bind(this);
  }

  componentDidMount(){
    this.loadIt( this );
  }

  loadIt( pm ){
    this.todoService.getAllTodos( pm );
  }

  mapTodos(el,idx,arr){
    return <Todo key={idx} todo={el} />
  }

  todoList(){
    return this.state.todos.map( this.mapTodos );
  }

  noOpLink(event){
    event.preventDefault(); 
  }

  render(){ 
    
    let element = (
      <>
        <div className="jumbotron">
          <div className="container">
            <h1 className="display-3">Todos List</h1>
            <p><NavLink className="btn btn-primary btn-lg" onClick={ this.noOpLink } to="#" role="button">Learn more &raquo;</NavLink></p>
          </div>
        </div>
  
        <div className="container">
  
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Description</th>
                <th>Responsible</th>
                <th>Priority</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <tbody>
              { this.todoList() }
            </tbody>
          </table>
  
        </div>
      </>
    );
    return element;
  }
}

export {TodosList};