import React from 'react';
import { useState } from 'react';
import { NavLink , useHistory } from 'react-router-dom';
import axios from 'axios';

// see this to include history...
// https://reacttraining.com/react-router/core/api/Hooks/usehistory

const CreateTodo = () => { 

  let history = useHistory(); // console.log( "history" , history );

  const [ todo_description, setTodo_description ] = useState('');
  const [ todo_responsible, setTodo_responsible ] = useState('');
  const [ todo_priority, setTodo_priority ] = useState('');

  const noOpLink = (event) => { event.preventDefault(); };

  const massageThePhrase = (str) => {
    let offthefrontandback = str.trim();
    // let newstring = offthefrontandback.replace(/\s\s+/g, ' '); // alt 1
    let newstring = offthefrontandback.replace(/  +/g, ' '); // alt 2
    const firstChar = newstring.slice(0,1).toUpperCase();
    const otherChars = newstring.slice(1).toLowerCase();
    newstring = firstChar + otherChars;
    return newstring;
  };

  const onChangeTodoDescription = ( event ) => { setTodo_description( event.target.value ); };
  
  const onChangeTodoResponsible = ( event ) => { setTodo_responsible( event.target.value ); };
  
  const onChangeTodoPriority = ( event ) => { setTodo_priority( event.target.value ); };

  const handleSubmit = ( event ) => {

    event.preventDefault();

    // console.log(`Form submitted ~ Todo Description: ${ todo_description }`);
    // console.log(`Form submitted ~ Todo Responsible: ${ todo_responsible }`);
    // console.log(`Form submitted ~ Todo Priority: ${ todo_priority }`);

    const newTodo = {
      todo_description: massageThePhrase(todo_description)
      , todo_responsible: massageThePhrase(todo_responsible)
      , todo_priority: todo_priority
    };
    
    let baseUrl = "http://localhost:4000/todos"; 
    axios.post( `${ baseUrl}/add` , newTodo )
    .then( ( /* response */ )  => { 
      // console.log( "response.data" , response.data );
      history.push('/');
    } )
    .catch( (err) => console.log( "err", err ) ); 
    
  };

  let element = (
    <>
      <div className="jumbotron">
        <div className="container">
          <h1 className="display-3">Create New Todo</h1>
          <p><NavLink className="btn btn-primary btn-lg" onClick={ noOpLink } to="#" role="button">Learn more &raquo;</NavLink></p>
        </div>
      </div>

      <div className="container">

        <form onSubmit={ handleSubmit }>
          
          <div className="form-group">
          <label htmlFor="description" className="form-check-label">Description</label>
            <input type="text" id="description" className="form-control" value={ todo_description } onChange={ onChangeTodoDescription } />
          </div>

          <div className="form-group">
            <label htmlFor="responsibility" className="form-check-label">Responsible</label>
            <input type="text" id="responsibility" className="form-control" value={ todo_responsible } onChange={ onChangeTodoResponsible } />
          </div>

          <div className="form-group">
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="radio" name="priorityOptions" id="priorityLow" value="Low" checked={ todo_priority==="Low"} onChange={ onChangeTodoPriority } />
              <label htmlFor="priorityLow" className="form-check-label">Low</label>
            </div>

            <div className="form-check form-check-inline">
              <input className="form-check-input" type="radio" name="priorityOptions" id="priorityMedium" value="Medium" checked={ todo_priority==="Medium"} onChange={ onChangeTodoPriority } />
              <label htmlFor="priorityMedium" className="form-check-label">Medium</label>
            </div>

            <div className="form-check form-check-inline">
              <input className="form-check-input" type="radio" name="priorityOptions" id="priorityHigh" value="High" checked={ todo_priority==="High"} onChange={ onChangeTodoPriority } />
              <label htmlFor="priorityHigh" className="form-check-label">High</label>
            </div>
          </div>

          <div className="form-group">
            <input type="submit" value="Create Todo" className="btn btn-primary" disabled={ todo_description === '' || todo_responsible === '' || todo_priority === '' } />
          </div>

        </form>

      </div>
    </>
  );

  return element;
};

export { CreateTodo };