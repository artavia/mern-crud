import React , { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import axios from 'axios';

const EditTodo = ( props ) => {

  // console.log( "props" , props );
  // console.log( "props.match.params.id" , props.match.params.id );
  
  const [ todo_description, setTodo_description ] = useState('');
  const [ todo_responsible, setTodo_responsible ] = useState('');
  const [ todo_priority, setTodo_priority ] = useState('');
  const [ todo_completed , setTodo_completed ] = useState(''); 

  useEffect( () => {

    const fetchData = async () => {

      let id = props.match.params.id;
      let baseUrl = "http://localhost:4000/todos";
      const result = await axios.get( `${baseUrl}/${id}` );
      
      const { todo_description , todo_responsible, todo_priority , todo_completed } = result.data; 
      // console.log( "result.data" , result.data );

      setTodo_description( todo_description );
      setTodo_responsible( todo_responsible );
      setTodo_priority( todo_priority );
      setTodo_completed( todo_completed );
    }; 

    fetchData();

  } , [ props.match.params.id ] ); 

  const noOpLink = (event) => { event.preventDefault(); };

  const massageThePhrase = (str) => {
    let offthefrontandback = str.trim();
    // let newstring = offthefrontandback.replace(/\s\s+/g, ' '); // alt 1
    let newstring = offthefrontandback.replace(/  +/g, ' '); // alt 2
    const firstChar = newstring.slice(0,1).toUpperCase();
    const otherChars = newstring.slice(1).toLowerCase();
    newstring = firstChar + otherChars;
    return newstring;
  };

  const onChangeTodoDescription = ( event ) => { setTodo_description( event.target.value ); };
  
  const onChangeTodoResponsible = ( event ) => { setTodo_responsible( event.target.value ); };
  
  const onChangeTodoPriority = ( event ) => { setTodo_priority( event.target.value ); };

  const onChangeTodoCompleted = ( event ) => {
    setTodo_completed( !todo_completed );
  };

  const handleSubmit = (event) => {
    
    event.preventDefault();

    const todoObject = { todo_description: massageThePhrase(todo_description), todo_responsible: massageThePhrase(todo_responsible), todo_priority: todo_priority, todo_completed: todo_completed }; 
    // console.log( "todoObject" , todoObject );

    let baseUrl = "http://localhost:4000/todos";
    let id = props.match.params.id;

    axios.post( `${baseUrl}/update/${id}`, todoObject )
    .then( ( /* response */ ) => { 
      // console.log( "response.data" , response.data );
      props.history.push( "/" );
    } )
    .catch( (err) => console.log( "err", err ) ); 

  };

  let element = (
    <>
      <div className="jumbotron">
        <div className="container">
          <h1 className="display-3">Edit Todo Item</h1>   
          <p><NavLink className="btn btn-primary btn-lg" onClick={ noOpLink } to="#" role="button">Learn more &raquo;</NavLink></p>
        </div>
      </div>

      <div className="container">

      <form onSubmit={ handleSubmit }>
          
          <div className="form-group">
          <label htmlFor="description" className="form-check-label">Description</label>
            <input type="text" id="description" className="form-control" value={ todo_description } onChange={ onChangeTodoDescription } />
          </div>

          <div className="form-group">
            <label htmlFor="responsibility" className="form-check-label">Responsible</label>
            <input type="text" id="responsibility" className="form-control" value={ todo_responsible } onChange={ onChangeTodoResponsible } />
          </div>

          <div className="form-group">
            <div className="form-check form-check-inline">
              <input className="form-check-input" type="radio" name="priorityOptions" id="priorityLow" value="Low" checked={ todo_priority==="Low"} onChange={ onChangeTodoPriority } />
              <label htmlFor="priorityLow" className="form-check-label">Low</label>
            </div>

            <div className="form-check form-check-inline">
              <input className="form-check-input" type="radio" name="priorityOptions" id="priorityMedium" value="Medium" checked={ todo_priority==="Medium"} onChange={ onChangeTodoPriority } />
              <label htmlFor="priorityMedium" className="form-check-label">Medium</label>
            </div>

            <div className="form-check form-check-inline">
              <input className="form-check-input" type="radio" name="priorityOptions" id="priorityHigh" value="High" checked={ todo_priority==="High"} onChange={ onChangeTodoPriority } />
              <label htmlFor="priorityHigh" className="form-check-label">High</label>
            </div>
          </div>

          <div className="form-group">
            <div className="form-check form-check-inline">
              <input type="checkbox" className="form-check-input" id="completedCheckbox" name="completedCheckbox" onChange={ onChangeTodoCompleted } checked={ todo_completed } value={ todo_completed } />
              <label htmlFor="completedCheckbox" className="form-check-label">Completed</label>
            </div>
          </div>

          <div className="form-group">
            <input type="submit" value="Update Todo" className="btn btn-warning" disabled={ todo_description === '' || todo_responsible === '' || todo_priority === '' } />
          </div>

        </form>

      </div>
    </>
  );
  return element;
};

export {EditTodo};