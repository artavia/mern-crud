import React , { useState } from 'react';
import { NavLink, Redirect } from 'react-router-dom';
import axios from 'axios';

const TableRow = ( props ) => {

  // console.log( "props", props ); 
  // console.log( "props.todo._id", props.todo._id ); 

  const [ redirect, setRedirect ] = useState( false );
  
  const deleteRecord = (event) => {

    event.preventDefault();

    let id = props.todo._id;
    let baseUrl = "http://localhost:4000/todos";

    axios.get( `${ baseUrl }/delete/${id}` )
    .then( (/* response */) => {
      // console.log( "response.data", response.data );
      setRedirect( true );
    } )
    .catch( (err) => console.log( "err", err ) );
  };

  if( redirect ){
    return ( <Redirect to='/' /> );
  }

  let element = (
    <tr>
      <td className={ props.todo.todo_completed ? 'completed' : '' } >{props.todo.todo_description}</td>
      <td className={ props.todo.todo_completed ? 'completed' : '' }>{props.todo.todo_responsible}</td>
      <td className={ props.todo.todo_completed ? 'completed' : '' }>{props.todo.todo_priority}</td>
      <td>
        <NavLink to={`/edit/${props.todo._id}`}>
          Edit
        </NavLink>
      </td>
      <td>
        <NavLink className="danger" onClick={ deleteRecord } to="#" >
          Delete
        </NavLink>
      </td>
    </tr>
  );
  return element; 

};

export { TableRow };