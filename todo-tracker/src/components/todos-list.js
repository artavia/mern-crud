import React from 'react';
import { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import { TableRow } from './table-row';
import axios from 'axios';

const TodosList = () => {

  // console.log( "this", this ); // undefined
  // console.log( "React", React ); // yada, yada, yada

  const [ todos, setTodos ] = useState( [] );

  useEffect( () => {

    const fetchData = async () => {
      let baseUrl = "http://localhost:4000/todos";
      const result = await axios.get( baseUrl );
      setTodos( result.data );
    }; 

    fetchData();

  } , [] ); 

  const todoList = () => {
    return todos.map( mapTodos );
  };

  const mapTodos = (el,idx,arr) => {
    return <TableRow key={idx} todo={el} />
  };

  const noOpLink = (event) => {
    event.preventDefault(); 
  };

  let element = (
    <>
      <div className="jumbotron">
        <div className="container">
          <h1 className="display-3">Todos List</h1>
          <p><NavLink className="btn btn-primary btn-lg" onClick={ noOpLink } to="#" role="button">Learn more &raquo;</NavLink></p>
        </div>
      </div>

      <div className="container">

        <table className="table table-striped">
          <thead>
            <tr>
              <th>Description</th>
              <th>Responsible</th>
              <th>Priority</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            { todoList() }
          </tbody>
        </table>

      </div>
    </>
  );
  return element;
};


export { TodosList };