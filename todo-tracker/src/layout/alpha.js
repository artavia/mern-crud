import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import { TodosList } from "../components/todos-list";
import { CreateTodo } from "../components/create-todo";
import { EditTodo } from "../components/edit-todo";
import { Main } from './main';

// Robin otherwise killed it... BUT IT FELL SHORT
// https://www.robinwieruch.de/react-pass-props-to-component

// therefore, see this...
// https://reacttraining.com/react-router/core/api/Hooks/usehistory

const Alpha = () => {
  
  let element = ( 
    <BrowserRouter>
      <Main>
        <Switch>
          <Route path="/" exact component={TodosList} />
          <Route path="/create" component={CreateTodo} />
          <Route path="/edit/:id" component={EditTodo} />
        </Switch>
      </Main>
    </BrowserRouter>
  );
  
  return element;

};

export {Alpha};