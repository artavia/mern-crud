import React from 'react';

import { Nav } from "./nav";
import { Footer } from "./footer";

const Main = ( props ) => {
  
  // console.log( 'Main' , props );
  // console.log( 'Main' , props.children );
  // console.log( 'Main' , props.children._owner );
  // console.log( 'Main' , props.children._self );
  // console.log( 'Main' , props.children._source );
  // console.log( 'Main' , props.children._store ); 
  // console.log( 'Main' , props.children.key ); // null
  // console.log( 'Main' , props.children.props );
  // console.log( 'Main' , props.children.ref ); // null
  // console.log( 'Main' , props.children.type );
  
  let element = (
    <div className="container">
      <Nav />
      <main role="main">
        { props.children }
      </main>
      <Footer />
    </div>
  );

  return element;
};

export {Main};