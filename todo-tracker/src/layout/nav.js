import React from 'react';
import { NavLink } from 'react-router-dom';

const Nav = () => {
  
  const noOpLink = (event) => { event.preventDefault(); };

  let element = (
    <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      
      <NavLink className="navbar-brand" to="/" onClick={ noOpLink }>MERN Todo App</NavLink>
      
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul className="navbar-nav mr-auto">
          
          <li className="nav-item">
            <NavLink className="nav-link" to={"/"} activeClassName={"active"} exact>
              Todos
            </NavLink>
          </li>
          
          <li className="nav-item">
            <NavLink className="nav-link" to={"/create"} activeClassName={"active"}>
              Create Todo
            </NavLink>
          </li>

        </ul>
      </div>

    </nav>
  );
  return element;
};

export {Nav};